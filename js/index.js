window.onscroll = function () {
  if (
    document.body.scrollTop >= 150 ||
    document.documentElement.scrollTop >= 150
  ) {
    document.querySelector('.navbar').style.backgroundColor = "#181818";
    document.querySelector('.scroll-up').style.opacity = 1
  } else {
    document.querySelector('.navbar').style.backgroundColor = "transparent";
    document.querySelector('.scroll-up').style.opacity = 0
  }
};

//Animation
AOS.init();

//Footer
document.querySelector('.copyright').innerHTML = `&copy; ${new Date().getFullYear()} Gleam.`

const album = document.querySelector('#album');
const blog = document.querySelector('.blog');
const contact = document.querySelector('.contact-item');
const btn_up = document.querySelector('.scroll-up')
const imageTabBtn = document.querySelector('.album-tab-image')
const videoTabBtn = document.querySelector('.album-tab-video')

btn_up.addEventListener('click', () => {
  window.scroll({
    top: 0,
    left: 0,
    behavior: "smooth"
  })
})

const images = [
  {
    image: [
      'https://picsum.photos/300/200?image=232',
      'https://picsum.photos/300/200?image=231',
      'https://picsum.photos/300/200?image=211',
      'https://picsum.photos/300/200?image=201',
      'https://picsum.photos/300/200?image=240',
      'https://picsum.photos/300/200?image=141',
    ],
    name: 'Crazy Mood',
    category: 'people',
  },
  {
    image: [
      'https://picsum.photos/300/200?image=143',
      'https://picsum.photos/300/200?image=203',
      'https://picsum.photos/300/200?image=343',
      'https://picsum.photos/300/200?image=233',
      'https://picsum.photos/300/200?image=253',
      'https://picsum.photos/300/200?image=153',
    ],
    name: 'Crazy Mood',
    category: 'animal',
  },
  {
    image: [
      'https://picsum.photos/300/200?image=240',
      'https://picsum.photos/300/200?image=343',
      'https://picsum.photos/300/200?image=233',
      'https://picsum.photos/300/200?image=253'
    ],
    name: 'Crazy Mood',
    category: 'wedding',
  },
  {
    image: [
      'https://picsum.photos/300/200?image=244',
      'https://picsum.photos/300/200?image=444',
      'https://picsum.photos/300/200?image=264',
      'https://picsum.photos/300/200?image=284',
      'https://picsum.photos/300/200?image=294',
    ],
    name: 'Crazy Mood',
    category: 'culture',
  },
  {
    image: [
      'https://picsum.photos/300/200?image=235',
      'https://picsum.photos/300/200?image=135',
      'https://picsum.photos/300/200?image=335',
      'https://picsum.photos/300/200?image=435',
      'https://picsum.photos/300/200?image=230',
    ],
    name: 'Crazy Mood',
    category: 'sport',
  },
  {
    image: [
      'https://picsum.photos/300/200?image=236',
      'https://picsum.photos/300/200?image=216',
      'https://picsum.photos/300/200?image=217',
      'https://picsum.photos/300/200?image=256',
      'https://picsum.photos/300/200?image=276',
      'https://picsum.photos/300/200?image=296',
    ],
    name: 'Crazy Mood',
    category: 'school',
  },
  {
    image: [
      'https://picsum.photos/300/200?image=237',
      'https://picsum.photos/300/200?image=337',
      'https://picsum.photos/300/200?image=537',
      'https://picsum.photos/300/200?image=287',
      'https://picsum.photos/300/200?image=297',
      'https://picsum.photos/300/200?image=217',
    ],
    name: 'Crazy Mood',
    category: 'graduation',
  },
];

const videos = [
  {
    cover: 'https://images.unsplash.com/photo-1469371670807-013ccf25f16a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8bWFycmlhZ2V8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
    album: [
      'https://www.youtube.com/embed/r00p_UmDhxU',
      'https://www.youtube.com/embed/r00p_UmDhxU',
      'https://www.youtube.com/embed/WhlrJH9j-Zw'
    ],
    category: "Marriage"
  },
  {
    cover: 'https://images.unsplash.com/photo-1557801200-9a8d901ded2a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjR8fGdyYWR1YXRpb258ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
    album: [
      'https://www.youtube.com/embed/HJ63BrQ85dc',
      'https://www.youtube.com/embed/oxZT8iIk7h8',
      'https://www.youtube.com/embed/oxZT8iIk7h8'
    ],
    category: "Graduation"
  },
  {
    cover: 'https://images.unsplash.com/photo-1530800919758-f1f7b1e67cd8?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTd8fGJpcnRoZGF5fGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
    album: [
      'https://www.youtube.com/embed/nl62hhiBMOM',
      'https://www.youtube.com/embed/oxZT8iIk7h8',
      'https://www.youtube.com/embed/oxZT8iIk7h8'
    ],
    category: "Birthday"
  }
]

const articles = [
  {
    image: 'https://picsum.photos/300/200?image=218',
    title: 'New Dress Collection',
    time: 'MARCH 10, 2021 . NEWS PORTRAITS',
    text: 'Arnare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et ...',
    },
    {
        image: 'https://picsum.photos/300/200?image=215',
        title: 'New Dress Collection',
        time: 'MARCH 10, 2021 . NEWS PORTRAITS',
        text: 'Arnare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et ...',
    },
    {
        image: 'https://picsum.photos/300/200?image=216',
        title: 'New Dress Collection',
        time: 'MARCH 10, 2021 . NEWS PORTRAITS',
        text: 'Arnare varius mauris eu commodo. Aenean nibh risus, rhoncus eget consectetur ac. Consectetur adipiscing elit. Vivamus auctor condimentum sem et ...',
    },

];

const contacts = [
  {
    title: "Address",
    value: "Kimihurura - Kigali City, KG 120 St"
  },
  {
    title: "Email",
    value: "example@gmail.com"
  },
  {
    title: "Phone",
    value: "+250786565657"
  }
]

function displayImages() {
  if (videoTabBtn !== null) {
    videoTabBtn.classList.remove('active')
    imageTabBtn.classList.add('active')
  }
  images.map((img) => {
    let card = `
        <div class="image" data-aos="zoom-in" data-aos-duration="2000">
          <a href="album.html?album=${img.category}" class="category">
            <figure><img src="${img.image[0]}" /></figure>
            <h2>${img.name}</h2>
            <span>${img.category}</span>
          </a>
        </div>
    `;
    if (album !== null) {
      album.innerHTML += card;
    }
  });
}
displayImages()

if (imageTabBtn !== null) {
  imageTabBtn.onclick = function () {
    album.innerHTML = ""
    displayImages()
  }
}

if (videoTabBtn !== null) {
  videoTabBtn.onclick = function () {
    videoTabBtn.classList.add('active')
    imageTabBtn.classList.remove('active')
    album.innerHTML = ""
    videos.map(item => {
      let card = `
    <div class="video" data-aos="zoom-in" data-aos-duration="2000">
      <iframe width="350" allowfullscreen height="300" src="${item.album[0]}">
      </iframe>
      <a href="album.html?video=${item.category}">More ${item.category} Video</a>
    </div>
  `
      if (album !== null) {
        album.innerHTML += card;
      }
    })
  }
}

articles.map((article) => {
    let card = `
        <div class="blog-card" data-aos="zoom-in" data-aos-duration="2000">
            <figure><img src="${article.image}" alt="" /></figure>
            <h2>${article.title}</h2>
            <div class="blog-time">
                <span>${article.time}</span>
            </div>
            <p>${article.text}</p>
        </div>
    `;
  if (blog !== null) {
    blog.innerHTML += card;
  }
});

contacts.map((item) => {
  let card = `
    <div class="item">
      <h2>${item.title}</h2>
      <p>${item.value}</p>
    </div>
  `
  if (contact !== null) {
    contact.innerHTML += card
  }
})

var splideLists = document.querySelector('.splide__list')
articles.map((article) => {
  let card = `
        <li class="blog-card splide__slide"  data-aos="zoom-in" data-aos-duration="2000">
            <figure><img src="${article.image}" alt="" /></figure>
            <h2>${article.title}</h2>
            <div class="blog-time">
                <span>${article.time}</span>
            </div>
            <p>${article.text}</p>
        </li>
    `;
  if (splideLists !== null) {
    splideLists.innerHTML += card;
  }
});

document.addEventListener("DOMContentLoaded", function () {
  global_carousel__ctrl();
});

//alternate way of triggering carousels in muiltiple instances
function global_carousel__ctrl() {
  var elms = document.getElementsByClassName("splide");
  for (var i = 0; i < elms.length; i++) {
    new Splide(elms[i], {
      type: "loop",
      perPage: 3,
      perMove: 1,
      autoplay: false,
      keyboard: "focused", //enabling keyboard for focused element
      gap: 50,
      fixedWidth: "20rem",
      pagination: false
    }).mount();
  }
}
