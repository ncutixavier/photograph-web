(function () {
  var albumTitle = document.querySelector('.album-title');
  var albumImages = document.querySelector('#album-images');
  var albumPage = document.querySelector('.album-page');

  if (!/(album=|video=)/.test(window.location.href)) {
    window.location.href = 'index.html';
  } else if (/album=/.test(window.location.href)) {
    let album = window.location.href.split('?')[1].split('=')[1];
    albumTitle.innerHTML = album;
    let album_images = images.find((img) => img.category == album);

    if (album_images !== undefined) {
      albumPage.style.backgroundImage = `url(${album_images.image[0]}), linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)`;
      album_images.image.map((img) => {
        let card = `
                <div class="image" data-aos="zoom-in" data-aos-duration="2000">
                    <a class="category">
                        <figure><img src="${img}" /></figure>
                    </a>
                </div>
            `;
        if (albumImages !== null) {
          albumImages.innerHTML += card;
        }
      })
    } else {
        document.getElementsByClassName('title').innerHTML = "Album has not Found"
    }
  } else if (/video=/.test(window.location.href)) {
    let album = window.location.href.split('?')[1].split('=')[1];
    albumTitle.innerHTML = album;
    let album_video = videos.find((item) => item.category == album);

    if (album_video !== undefined) {
      albumPage.style.backgroundImage = `url(${album_video.cover}), linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)`;
      album_video.album.map((item) => {
        let card = `
          <div class="video" data-aos="zoom-in" data-aos-duration="2000">
            <iframe width="350" allowfullscreen height="300" src="${item}">
            </iframe>
          </div>
            `;
        if (albumImages !== null) {
          albumImages.innerHTML += card;
        }
      })
    } else {
      document.getElementsByClassName('title').innerHTML = "Album has not Found"
    }
  }
})();
